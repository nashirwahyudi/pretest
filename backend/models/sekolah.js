'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Sekolah extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Sekolah.hasMany(models.Kelas, {
        foreignKey: 'id_sekolah',
        as: 'kelas'
      });
      Sekolah.hasMany(models.Siswa, {
        foreignKey: 'id_sekolah',
        as: 'siswa'
      });
    }
  }
  Sekolah.init({
    nama: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Sekolah',
  });
  return Sekolah;
};